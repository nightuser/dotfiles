let g:neomake_c_enabled_makers = ['clangtidy', 'clangcheck', 'cppcheck']

" GNU coding style
function GNUStyleLocal()
  setlocal cindent
  setlocal cinoptions=>4,n-2,{2,^-2,:2,=2,g0,h2,p5,t0,+2,(0,u0,w1,m1
  setlocal shiftwidth=2
  setlocal textwidth=79
  setlocal fo+=l
endfunction
function GNUStyle()
  augroup cgnustyle
    autocmd!
    autocmd FileType c call GNUStyleLocal()
  augroup END
  call GNUStyleLocal()
endfunction
command! GNUStyle call GNUStyle()
