" vim: set et ts=2 sw=2:

call plug#begin(stdpath('data') . '/plugged')
Plug 'rakr/vim-one'
Plug 'junegunn/seoul256.vim'
"Plug 'neomake/neomake'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'tpope/vim-fugitive'
Plug 'editorconfig/editorconfig-vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'neovim/nvim-lspconfig'
Plug 'lervag/vimtex', { 'for': ['tex', 'plaintex', 'context'] }
"Plug 'ncm2/float-preview.nvim'
Plug 'embear/vim-localvimrc'
call plug#end()

language en_US.UTF-8

set hidden
set laststatus=2 " 1 doesn't show filenames
set expandtab tabstop=8 shiftwidth=2
set wildmenu wildmode=full
set number relativenumber
set ignorecase smartcase hlsearch
let mapleader = "\<space>"
let maplocalleader = ","
set completeopt=menu,longest,noselect
set mouse=a
"set spelllang=en_us,ru_ru
set spelllang=en_us
set keymap=russian-jcukenwin
set iminsert=0 imsearch=0
set list listchars=tab:»·,trail:·
set splitright splitbelow
set shortmess-=F

let g:python3_host_prog = '/usr/bin/python3'

" Key bindings
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Theme
if $TERM_PROGRAM !=# 'Apple_Terminal'
  set termguicolors
endif

"let g:one_allow_italics = 1
"colorscheme one

let g:seoul256_background = 256
colorscheme seoul256

set background=light
"hi Normal ctermbg=NONE
hi LineNr ctermbg=256
"highlight Comment cterm=italic gui=italic

" Neomake
"call neomake#configure#automake('w')

" UltiSnips
let g:UltiSnipsSnippetDirectories=['UltiSnips', 'mysnippets']
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" Trim Whitespace
fun! TrimWhitespace()
  let l:save = winsaveview()
  keeppatterns %s/\s\+$//e
  call winrestview(l:save)
endfun
command! TrimWhitespace call TrimWhitespace()
" Don't trim them automatically, better do it with git-hook or something like
" that.
"augroup WritePreGrp
"  autocmd!
"  autocmd BufWritePre * :call TrimWhitespace()
"augroup END

" fzf
command! -bang -nargs=* Rg call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>), 1, <bang>0)
nmap <leader><tab> <plug>(fzf-maps-n)
xmap <leader><tab> <plug>(fzf-maps-x)
omap <leader><tab> <plug>(fzf-maps-o)
imap <c-x><c-f> <plug>(fzf-complete-path)
imap <c-x><c-j> <plug>(fzf-complete-file-ag)
imap <c-x><c-l> <plug>(fzf-complete-line)
noremap <leader>p :Files<CR>
noremap <leader>b :Buffers<CR>
noremap <leader>r :Rg<CR>

"" vim-lsc
"let g:lsc_server_commands =
"\ {
"\   'c': {
"\     'command': 'ccls',
"\     'suppress_stderr': v:true,
"\   },
"\   'cpp': {
"\     'command': 'ccls',
"\     'suppress_stderr': v:true,
"\   },
"\   'objcpp': {
"\     'command': 'ccls',
"\     'suppress_stderr': v:true,
"\   },
"\   'go': {
"\     'command': 'gopls serve',
"\     'log_level': -1,
"\     'suppress_stderr': v:true,
"\   },
"\   'ruby': {
"\     'command': 'solargraph stdio',
"\     'suppress_stderr': v:true,
"\   },
"\ }
""\   'python': {
""\     'command': 'pyls',
""\     'workspace_config': {
""\       'pyls': {
""\         'configurationSources': "['flake8']",
""\       },
""\     },
""\   },
"let g:lsc_enable_autocomplete = v:false
"let g:lsc_auto_map = {'defaults': v:true, 'Completion': 'omnifunc'}

" Tex
let g:tex_flavor = 'latex'
let g:vimtex_indent_enabled = 0
let g:vimtex_indent_on_ampersands = 0
let g:vimtex_syntax_nospell_comments = 1
let g:vimtex_view_method = 'skim'
let g:vimtex_compiler_latexmk =
\ {
\   'build_dir': 'dist',
\ }
"let g:vimtex_grammar_vlty =
"\ {
"\   'lt_command': 'languagetool',
"\   'server': 'my',
"\   'show_suggestions': 1
"\ }

" float-preview.nvim
let g:float_preview#docked = 1

" Local .vimrc
let g:localvimrc_name = ['.vimrc', '.lvimrc']
" let g:localvimrc_file_directory_only = 1
let g:localvimrc_persistent = 1
let g:localvimrc_persistence_file = stdpath('config') . '/localvimrc_persistent'
let g:localvimrc_python2_enable = 0

" LanguageTool
" let g:languagetool_jar = '/usr/share/java/languagetool/languagetool-commandline.jar'
"let g:languagetool_cmd='/usr/bin/languagetool'
"let g:languagetool_enable_categories = 'PUNCTUATION,TYPOGRAPHY,CASING,COLLOCATIONS,CONFUSED_WORDS,CREATIVE_WRITING,GRAMMAR,MISC,MISUSED_TERMS_EU_PUBLICATIONS,NONSTANDARD_PHRASES,PLAIN_ENGLISH,TYPOS,REDUNDANCY,SEMANTICS,TEXT_ANALYSIS,STYLE,GENDER_NEUTRALITY'
"let g:languagetool_enable_rules = 'AND_ALSO,ARE_ABLE_TO,ARTICLE_MISSING,AS_FAR_AS_X_IS_CONCERNED,BEST_EVER,BLEND_TOGETHER,BRIEF_MOMENT,CAN_NOT,CANT_HELP_BUT,COMMA_WHICH,EG_NO_COMMA,ELLIPSIS,EXACT_SAME,HONEST_TRUTH,HOPEFULLY,IE_NO_COMMA,IN_ORDER_TO,I_VE_A,NEGATE_MEANING,PASSIVE_VOICE,PLAN_ENGLISH,REASON_WHY,SENT_START_NUM,SERIAL_COMMA_OFF,SERIAL_COMMA_ON,SMARTPHONE,THREE_NN,TIRED_INTENSIFIERS,ULESLESS_THAT,WIKIPEDIA,WORLD_AROUND_IT'

augroup lsp_register
  au!
  au User lsp_setup call lsp#register_server({
    \ 'name': 'solargraph',
    \ 'cmd': {server_info->['solargraph', 'stdio']},
    \ 'allowlist': ['ruby'],
    \ 'config': { 'diagnostics': v:false }
    \ })
augroup END

function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    "setlocal signcolumn=yes
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gs <plug>(lsp-document-symbol-search)
    nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> [g <plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)
    nnoremap <buffer> <expr><c-f> lsp#scroll(+4)
    nnoremap <buffer> <expr><c-d> lsp#scroll(-4)

    "let g:lsp_format_sync_timeout = 1000
    "autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')
endfunction

augroup lsp_install
  au!
  au User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END

lua << EOF
local nvim_lsp = require('lspconfig')

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  -- Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap=true, silent=true }

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap('n', '<space>e', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
  buf_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
  buf_set_keymap('n', '<space>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)
  buf_set_keymap('n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)

  -- vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  --     vim.lsp.diagnostic.on_publish_diagnostics, {
  --         virtual_text = false
  --     }
  -- )
end

-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
local servers = { 'solargraph' }
for _, lsp in ipairs(servers) do
  nvim_lsp[lsp].setup {
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    }
  }
end
EOF
